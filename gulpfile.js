'use strict';
//The Gulp
const gulp = require('gulp');

//Packages without "gulp-" prefix
const babelify = require('babelify');
const browserify = require('browserify');
const vinylSourceStream = require('vinyl-source-stream');
const vinylBuffer = require('vinyl-buffer');

// Load all gulp plugins with "gulp-" prefix into the plugins object.
const plugins = require('gulp-load-plugins')();

// Folder Structure
const srcFolder = 'src/';
const appFolder = 'app/';
const srcAppPath = srcFolder + appFolder;
const appFilename = 'app';
const index = 'index.html';
const vendorFolder = 'vendor';
const dataFolder = 'data';
const tempFolder = 'temp';
const buildFolder = 'dist/';
const testPrefix = 'spec';
const remarkAssetsFolder = 'remark/assets';

const sys = {
    html: '/**/*.html',
    js: '/**/*.js',
    all: '/**',
    allin: '/**/*',
};
const src = {
    html: {
        all: srcAppPath + sys.html,
        min: srcFolder + tempFolder
    },
    js: {
        tpl: srcAppPath,
        all: srcAppPath + sys.js,
        main: srcAppPath + appFilename + '.js'
    },
    data: srcFolder + dataFolder + sys.allin + '.*',
    vendor: srcFolder + vendorFolder + sys.all,
};

const dist = {
    js: {
        main: appFilename + '.min.js',
        folder: buildFolder + appFolder
    },
    data: buildFolder + dataFolder,
    vendor: buildFolder + vendorFolder,
    assets: buildFolder + 'assets'
};

// Tmplate Cache config
const templates = {
    tplCache: {
        file: 'templates.js',
        options: {
            module: 'templates',
            root: appFolder,
            standalone: true
        }
    },
};

/* Tasks */
gulp.task('html', function () {
    return gulp.src(srcFolder + index)
        .pipe(gulp.dest(buildFolder))
        .pipe(plugins.htmlmin({collapseWhitespace: true}))
        .pipe(plugins.connect.reload());
});

// Minify HTML
gulp.task('html:minify', function () {
    return gulp.src([src.html.all, '!' + srcFolder + index])
        .pipe(plugins.htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(src.html.min)); //+"/app"
});

// Copy Remark
gulp.task('remark:move', function () {
    return gulp.src(remarkAssetsFolder + sys.allin)
        .pipe(gulp.dest(dist.assets));
});

// Move data
gulp.task('data:move', function () {
    return gulp.src(src.data)
        .pipe(gulp.dest(dist.data))
        .pipe(plugins.connect.reload());
});

// Angular templatecache
gulp.task('templates', ['html:minify'], function () {
    return gulp.src([src.html.min + sys.html])
        .pipe(plugins.angularTemplatecache(
            templates.tplCache.file,
            templates.tplCache.options
        ))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(src.js.tpl));
});

/* The jshint task runs jshint with ES6 support. */
gulp.task('jshint', function () {
    return gulp.src([src.js.all, "!" + srcAppPath + sys.allin + testPrefix + ".js", "!" + srcAppPath + templates.tplCache.file])
        .pipe(plugins.jshint({
            esnext: true // Enable ES6 support
        }))
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

gulp.task('vendor', function () {
    /* In a real project you of course would use npm or bower to manage libraries. */
    return gulp.src(src.vendor)
        .pipe(gulp.dest(dist.vendor))
        .pipe(plugins.connect.reload());
});

/* Compile all script files into one output minified JS file. */
gulp.task('scripts', ['jshint'], function () {

    let sources = browserify({
        entries: srcAppPath + appFilename + '.js',
        debug: true // Build source maps
    })
        .transform(babelify.configure({
            // Babel configuration here
            // https://babeljs.io/docs/usage/options/
            presets: ["es2015"]
        }));

    return sources.bundle()
        .pipe(vinylSourceStream(dist.js.main))
        .pipe(vinylBuffer())
        .pipe(plugins.sourcemaps.init({
            loadMaps: true // Load the sourcemaps browserify already generated
        }))
        .pipe(plugins.ngAnnotate())
        .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write('./', {
            includeContent: true
        }))
        .pipe(gulp.dest(dist.js.folder))
        .pipe(plugins.connect.reload());

});


gulp.task('serve', ['build', 'watch'], function () {
    plugins.connect.server({
        root: buildFolder,
        port: 4242,
        livereload: true,
        fallback: buildFolder + index
    });
});

gulp.task('watch', function () {
    gulp.watch(src.vendor, ['vendor']);
    gulp.watch(srcFolder + index, ['html']);
    gulp.watch(src.html.all, ['templates']);
    gulp.watch(src.js.all, ['scripts']);
    gulp.watch(src.data, ['data:move']);
});

gulp.task('app.build', ['templates']);
gulp.task('build', ['app.build', 'html', 'vendor','data:move']);
gulp.task('default', ['serve']);
