/**
 * Created by Xiaomi on 2/22/2017.
 */

/*
 * asScrollable plugin directive from remark
 */

/* @ngInject */
export default function ASScrollableDirective($document) {

    return {
        restrict: 'AE',
        link: function (scope, element, attrs) {
            let $BODY = $($document[0].body);
            let api = {};
            let $el = $(element);
            $el.asScrollable({
                namespace: 'scrollable',
                skin: 'scrollable-inverse',
                direction: 'vertical',
                contentSelector: '>',
                containerSelector: '>'
            });
            // .data('asScrollable')
        }
    };
}
