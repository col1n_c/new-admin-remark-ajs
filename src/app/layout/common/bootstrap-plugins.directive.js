/**
 * Created by Xiaomi on 2/22/2017.
 */

/*
 * [data-toggle] plugins directive from remark
 */

/* @ngInject */
function BootstrapToggles($document) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            let $BODY = $($document[0].body);
            let $el = $(element);
            let togglesList = {
                "tooltip": () => {
                    $el.tooltip();
                },
                "popover": () => {
                    $el.popover();
                }
            };
            if (togglesList[attrs.toggle]) {
                togglesList[attrs.toggle]();
            }
        }
    };
}
/* @ngInject */
function BootstrapPlugins($document, $timeout) {

    return {
        restrict: 'A',
        scope:{
            "total":"="
        },
        link: function (scope, element, attrs) {
            // console.log("plugins:",attrs.plugin);
            // let $BODY = $($document[0].body);
            let $el = $(element);
            let api = {};
            let defaults = {};
            let options = {};

            console.log("$el:",$el);
            console.log("scope.total:",scope.total);

            function renderPeity(pluginName) {
                defaults = PluginPeity[pluginName].getDefaults();
                options = angular.extend(defaults, {skin: attrs.skin});
                new PluginPeity[pluginName]($el, options).render();
            }

            function renderTabs() {
                defaults = PluginTabs.default.getDefaults();
                options = angular.extend(defaults, {});
                new PluginTabs.default($el, options).render();
            }

            function renderPaginator() {
                defaults = PluginAspaginator.default.getDefaults();
                options = angular.extend(defaults, { total:attrs.total, skin:attrs.skin});
                new PluginAspaginator.default($el,options).render();
                api = $el.data('asPaginator');
                console.log("element:",$el,"\napi:",api);
            }

            scope.$watch(attrs.total, function(value) {
                console.log("Watching aspaginator:",$el,"\napi:",api);
                console.log("value:",value.length);
                console.log("scope:",scope.total);
                if(api){
                    console.log("api.destroy()", api.destroy);
                    options = angular.extend(defaults, { total:value.length });
                    new PluginAspaginator.default($el,options).render();
                }
            });

            function renderMatchHeight() {
                let defaults = PluginMatchheight.default.getDefaults();
                let options = angular.extend(defaults, {
                    byRow: attrs.byRow
                });
                let matchSelector = $el.data('matchSelector');

                if (matchSelector) {
                    $el.find(matchSelector).matchHeight(options);
                } else {
                    $el.children().matchHeight(options);
                }
                new PluginMatchheight.default($el, options).render();
            }

            let pluginsList = {
                "peityPie": () => {
                    renderPeity("PeityPie");
                },
                "peityBar": () => {
                    renderPeity("PeityBar");
                },
                "peityLine": () => {
                    renderPeity("PeityLine");
                },
                "peityDonut": () => {
                    renderPeity("PeityDonut");
                },
                "matchHeight": () => {
                    renderMatchHeight();
                },
                // "tabs": () => {
                //     renderTabs();
                // },
                "paginator": () => {
                    renderPaginator();
                }
            };

            if (pluginsList[attrs.plugin]) {
                pluginsList[attrs.plugin]();
            }
        }
    };
}
export {BootstrapToggles, BootstrapPlugins};
