/*
 You could also use a class for a controller, but if you work
 with $scope and not the controller-as syntax a function makes
 more sense, because most of the code would anyway go to the
 constructor if you would use a class.
 */

export default class MainController {

    /*@ngInject;*/
    constructor($state,$scope,PageProperties,siteLang) {
        let ctrl = this;
        this.hello = "hello";
        this.lang = siteLang?siteLang.data:{};
        console.log("Main CTRL controller", $state.current);
        $scope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
            // console.log("%cstate change started in MainController",'background-color: green;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
            // console.log("$arguments",arguments);
        });
        $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
            // console.log("%cstate change ended in MainController",'background-color: orange;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
            // controller.sectionClass = toState.stateClass;
            console.log("toState",toState);
            console.log("fromState",fromState);
            if(ctrl.lang){
                PageProperties.setClosestKnownProperties(toState.name,ctrl.lang.statesData);
                if (ctrl.lang.statesData[toState.name]) {
                    PageProperties.setProperties(ctrl.lang.statesData[toState.name]);
                }
            }
            // console.log("$arguments",arguments);
        });
        $scope.$on('$viewContentLoaded',function(event, toState, toParams, fromState, fromParams){
            // console.log("%cviewContentLoaded in MainController",'background-color: blue;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
            // console.log("$arguments",arguments);
        });
    }

}
