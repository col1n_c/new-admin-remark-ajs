/*
 * User list controller
 */

export default class UserListController {

    /*@ngInject;*/
    constructor(Auth, $state, $scope, $timeout, stateScipt) {
        console.log("UserListController controller");
        this.userList = [
            {
                name:'Pasha',
                surname:'K.',
                avatar:'../../assets/portraits/2.jpg'
            },
            {
                name:'Mary',
                surname:'Adams',
                avatar:'../../assets/portraits/3.jpg'
            },
            {
                name:'Caleb',
                surname:'Richards',
                avatar:'../../assets/portraits/4.jpg'
            },
            {
                name:'June',
                surname:'Lane',
                avatar:'../../assets/portraits/5.jpg'
            },
            {
                name:'Edward',
                surname:'Fletcher',
                avatar:'../../assets/portraits/6.jpg'
            }
        ];
    }
}
