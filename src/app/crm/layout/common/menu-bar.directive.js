/**
 * Created by Xiaomi on 2/22/2017.
 */

/**
 * Menubar init function from remark
 */

/* @ngInject */
export default function MenuBarDirective($document) {

    return {
        restrict: 'AE',
        link: function (scope, element, attrs) {
            let $BODY = $($document[0].body);
            let api = {};
            let $el = $(element);
            let $menuBody = "";
            let asSaApi = {};

            //Initiates mmenu plugin on menu and sets events
            $el.mmenu({
                offCanvas: false,
                navbars: [{
                    position: 'bottom',
                    content: [
                        `<div class="site-menubar-footer">
              <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Settings">
                <span class="icon md-settings" aria-hidden="true"></span>
              </a>
              <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                <span class="icon md-eye-off" aria-hidden="true"></span>
              </a>
              <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                <span class="icon md-power" aria-hidden="true"></span>
              </a>
            </div>`
                    ]
                }]
            }).on('mouseenter', () => {
                $BODY.addClass('site-menubar-hover');

                setTimeout(() => {
                    console.log("enabling");
                    asSaApi.enable();
                }, 500);

            }).on('mouseleave', () => {
                $BODY.removeClass('site-menubar-hover');

                api = $el.data("mmenu");
                if (api) {
                    api.openPanel($('#mm-0'));
                }
                setTimeout(() => {
                    asSaApi.disable();
                }, 500);
            });

            //Initiates asScrollable plugin inside and returns block API
            $menuBody = $el.children('.mm-panels');
            asSaApi = $menuBody.asScrollable({
                namespace: 'scrollable',
                skin: 'scrollable-inverse',
                direction: 'vertical',
                contentSelector: '>',
                containerSelector: '>'
            }).data('asScrollable');

            //Resets scrollable block size (just tu be sure everything looks fine at the beginning)
            asSaApi.disable();
        }
    };
}