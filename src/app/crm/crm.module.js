angular.module("app.crm")
    .config(crmConfig)
// .run(appRun)
// .controller('UserListController', UserListController)
;

/*@ngInject;*/
function crmConfig($stateProvider) {
    console.log("app.crm config");
    $stateProvider
        .state('app.crm.dash', {
            url: '^/dash',
            views: {
                'main@app': {
                    templateUrl: "app/crm/dash/dash.html",
                    controller: "DashController as dashCtrl"
                },
            },
            pageProp: {
                bodyClass: "dashboard",
                pageClass: "",
                showMenu: true,
                showNavbar: true,
                showFooter: true,
                customStyles: [
                    '/assets/examples/css/dashboard/v1.css',
                ]
            },
            resolve: {
                stateScipt: (angularLoad, $http) => {
                    console.log("resolving peity");
                    angularLoad.loadScript('/assets/js/Plugin/peity.js').then(function () {
                        console.log("resolved peity");
                        // Script loaded succesfully.
                        // We can now start using the functions from someplugin.js
                    }).catch(function () {
                        console.log("not resolved peity");
                        // There was some error loading the script. Meh
                    });
                    return angularLoad.loadScript('/assets/vendor/peity/jquery.peity.min.js').then(function () {
                        console.log("resolved peity");
                        // Script loaded succesfully.
                        // We can now start using the functions from someplugin.js
                    }).catch(function () {
                        console.log("not resolved peity");
                        // There was some error loading the script. Meh
                    });
                }
            }
        })
        .state('app.crm.user', {
            url: '^/user',
            abstract: true,
            views: {
                'main': {
                    templateUrl: "",
                    // controller: "HomeController as homeCtrl"
                }
            }
        })
    ;
}