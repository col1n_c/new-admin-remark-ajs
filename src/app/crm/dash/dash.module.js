import DashController from "./dash.controller";

angular.module("app.crm.dash")
    // .config(userConfig)
    // .run(appRun)
    .controller('DashController', DashController)
;
//
// /*@ngInject;*/
// function userConfig($stateProvider) {
//     console.log("app.user config");
//     $stateProvider
//         .state('app.crm.user.list', {
//             url: '/list',
//             views: {
//                 'main@app': {
//                     templateUrl: "app/crm/user/list.html",
//                     controller: "UserListController as usrListCtrl"
//                 }
//             },
//             pageProp: {
//                 bodyClass: "page-user",
//                 pageClass: "",
//                 showMenu: true,
//                 showNavbar: true,
//                 showFooter: true,
//                 customStyles: [
//                     "/assets/examples/css/pages/user.css",
//                 ]
//             },
//             resolve:{
//                 stateScipt: (angularLoad,$http) => {
//                     console.log("resolving stateScipt");
//
//                     // return $http.get('http://httpbin.org/delay/5');
//
//                     // return $http({
//                     //     method: 'GET',
//                     //     url: ('data/lang-list.json')
//                     // });
//
//                     angularLoad.loadScript('/assets/vendor/aspaginator/jquery.asPaginator.min.js')
//                         .then(function() {
//                         console.log("resolved asPaginator");
//                         // Script loaded succesfully.
//                         // We can now start using the functions from someplugin.js
//                     }).catch(function() {
//                         console.log("not resolved asPaginator");
//                         // There was some error loading the script. Meh
//                     });
//                     angularLoad.loadScript('/assets/js/Plugin/aspaginator.js').then(function() {
//                         console.log("resolved aspaginator");
//                         // Script loaded succesfully.
//                         // We can now start using the functions from someplugin.js
//                     }).catch(function() {
//                         console.log("not resolved aspaginator");
//                         // There was some error loading the script. Meh
//                     });
//                     angularLoad.loadScript('/assets/js/Plugin/tabs.js').then(function() {
//                         console.log("resolved tabs");
//                         // Script loaded succesfully.
//                         // We can now start using the functions from someplugin.js
//                     }).catch(function() {
//                         console.log("not resolved tabs");
//                         // There was some error loading the script. Meh
//                     });
//                     return angularLoad.loadScript('/assets/js/Plugin/responsive-tabs.js').then(function() {
//                         console.log("resolved responsive-tabs");
//                         // Script loaded succesfully.
//                         // We can now start using the functions from someplugin.js
//                     }).catch(function() {
//                         console.log("not resolved responsive-tabs");
//                         // There was some error loading the script. Meh
//                     });
//                 }
//             }
//         })
//     ;
// }