/**
 * Created by Xiaomi on 2/20/2017.
 */

describe('Unit testing hiding name', function() {
    beforeEach(module('app'));

    var $compile,
        $rootScope;

    beforeEach(inject(function(_$compile_, _$rootScope_){
        $compile = _$compile_;
        $rootScope = _$rootScope_;
    }));

    it('hides itself', function() {
        var element = $compile("<div name-dir=''></div>")($rootScope);
        $rootScope.$digest();
        expect(element[0].style.display).toMatch("none");
    });
});