/*

 */

export default class AccountController {

    /*@ngInject;*/
    constructor(accData, langList, $state, $scope) {
        let data = accData.data;
        let controller = this;

        this.sectionClass = $state.current.stateClass;
        this.langList = langList.data;
        this.userLang = langList.data[1];
        this._getAccData = function(){
            return data;
        };

        $scope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
            console.log("%cstate change started in AccountController",'background-color: green;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
        });
        $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
            console.log("%cstate change ended in AccountController",'background-color: orange;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
            controller.sectionClass = toState.stateClass;
        });
        $scope.$on('$viewContentLoaded',function(event, toState, toParams, fromState, fromParams){
            console.log("%cviewContentLoaded in AccountController",'background-color: blue;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
            console.log("$arguments",arguments);
        });
    }
}
