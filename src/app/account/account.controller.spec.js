/**
 * Trying to get into tests
 */

'use strict';

describe('testing empty', function () {
    beforeEach(module('app'));
    var $controller;

    beforeEach(inject(function (_$controller_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
    }));

    //
    describe('controller controller properties', function () {
        var $scope;
        var $state;
        var controller;
        var accData;
        var langList;
        var data;

        beforeEach(function () {
            $scope = {"$on":function(){return "test"}};
            $state = {"current":{"stateClass":"test"}};
            accData = {"data":"test"};
            langList = {"data":"test"};
            controller = $controller('AccountController',{accData:accData,langList:langList,$state:$state,$scope:$scope,});
            data = controller._getAccData();
        });

        it('should show if controller is defined', function () {
            expect(controller).toBeDefined();
        });

        it('should show if accData is present', function () {
            expect(data).toMatch("test");
        });

        it('should show if langList is present', function () {
            expect(controller.langList).toMatch("test");
        });

    });
});