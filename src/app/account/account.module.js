import AccountController from "./account.controller";

angular.module("app.account")
    .config(accountConfig)
    // .run(appRun)
    .controller('AccountController', AccountController)
;


/*@ngInject;*/
function accountConfig($stateProvider) {
    console.log("app.account config");
    $stateProvider
        .state('app.account.index', {
            url: '/my-profile',
            views: {
                'accountView': {
                    templateUrl: "app/account/my-profile.html",
                }
            },
            stateClass: "my-profile"
        })
        .state('app.account.address', {
            url: '/address',
            views: {
                'accountView': {
                    templateUrl: "app/account/address-book.html",
                }
            },
            stateClass: "address-book"
        })
        .state('app.account.favorites', {
            url: '/favorites',
            views: {
                'accountView': {
                    templateUrl: "app/account/favorites.html",
                }
            },
            stateClass: "favorites"
        })
        .state('app.account.my-orders', {
            url: '/my-orders',
            views: {
                'accountView': {
                    templateUrl: "app/account/my-orders.html",
                }
            },
            stateClass: "my-order"
        })
    ;
}