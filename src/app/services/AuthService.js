/*
 * Auth service
 */

export default class AuthService {

    /*@ngInject;*/
    constructor($state, $rootScope, $http, PageProperties, $document) {
        this.$http = $http;
        this.PageProperties = PageProperties;
        this.userLogged = true;

        let allStates = $state.get();
        this.listOfStates = [];
        for (let state in allStates) {
            if (allStates.hasOwnProperty(state)) {
                this.listOfStates.push(allStates[state].name);
            }
        }
        this.publicStates = [
            'app.index',
            'app.dash',
            'app.404',
            'app.account',
        ];
    }

    checkState(stateToCheck) {
        console.log("stateToCheck", stateToCheck);
        console.log("this.allStates", this.listOfStates);
        let statePresent = this.listOfStates.includes(stateToCheck);
        let stateAllowed = this.userLogged ? true : this.publicStates.includes(stateToCheck);
        return {statePresent, stateAllowed};
    }

    checkUserLogged() {
        return this.userLogged;
    }

    setCookie(cname, cvalue, exdays) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toGMTString();
        $document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    getCookie(cname) {
        let name = cname + "=";
        let ca = $document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    checkCookie(cookie) {
        let retCookie = this.getCookie(cookie);
        if (retCookie !== "") {
            return retCookie;
        } else {
            return false;
        }
    }

    Login(username, password, remember, callback) {
        this.$http.post(this.PageProperties.baseUrl + '/v1/auth', {username: username, password: password})
            .then(function (response) {
                // login successful if there's a token in the response
                // console.log('data response',response.data);
                if (response.data.success) {
                    // store username and token in local storage to keep user logged in between page refreshes
                    // let time = 0.0017;
                    let time = 1;
                    if (remember) {
                        time = 30;
                    }
                    this.setCookie('token', response.data.token, time);

                    this.PageProperties.logged = true;

                    // add jwt token to auth header for all requests made by the $http service
                    this.$http.defaults.headers.common.Authorization = 'Bearer ' + response.data.token;

                    // execute callback with true to indicate successful login
                    callback(true, response);
                } else {
                    // execute callback with false to indicate failed login
                    callback(false, response);
                }
            })
            .catch(function (response) {
                callback(false, response?response:false);
            });
    }

    Logout() {
        let demolish = () => {

            this.PageProperties.logged = false;
            this.setCookie('token', '', 0);

            this.$http.defaults.headers.common.Authorization = '';
        };

        demolish();
    }

}