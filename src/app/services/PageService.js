export default class PageService {

    /*@ngInject;*/
    constructor() {
        this._allowedProps = ["title", "bodyClass", "pageClass", "showMenu", "showNavbar", "showFooter", "customStyles"];
        this.pageProp = {};
        this.baseUrl = 'http://api.urbanteens.dev';
        this.logged = true;
    }

    bodyClass() {
        return this.pageProp.bodyClass || "";
    }

    pageClass() {
        return this.pageProp.pageClass || "";
    }

    customStyles() {
        return this.pageProp.customStyles || "";
    }

    showMenu() {
        return this.pageProp.showMenu || "";
    }

    showNavbar() {
        return this.pageProp.showNavbar || "";
    }

    showFooter() {
        return this.pageProp.showFooter || "";
    }

    title() {
        return this.pageProp.title || "";
    }

    layout() {
        let result = (this.pageProp.showMenu && this.pageProp.showNavbar) ? false : true;
        return result;
    }

    getProperties() {
        return this.pageProp;
    }

    setClosestKnownProperties(stateName, list) {
        let statesInChain = stateName.split(".");
        let sw = true;
        let i = 0;
        do {
            statesInChain.pop();
            i++;
            let parentState = statesInChain.join(".");
            if (list[parentState]) {
                sw = false;
                this.setProperties(list[parentState]);
            }
        }
        while ((i < statesInChain.length) && sw);
    }

    /*
     Setting service properties
     Single or multiple
     */

    setProperties(property, ...value) {
        let answer = "Not allowed";
        if (typeof property == "string" && value) {
            if (this._allowedProps.includes(property)) {
                this.pageProp[property] = value;
                answer = "Set.";
            }
        }
        if (typeof property == "object") {
            console.log("property", property);
            // this.pageProp = {};
            for (let prop in property) {
                console.log("prop", prop);
                if (this._allowedProps.includes(prop)) {
                    console.log("allowed prop", prop);
                    this.pageProp[prop] = property[prop];
                }
            }
            answer = "Set.";
        }
        return answer;
    }
}