import LoginController from "./login.controller";

angular.module("app.auth")
    .config(authConfig)
    // .run(appRun)
    .controller('LoginController', LoginController)
;

/*@ngInject;*/
function authConfig($stateProvider) {
    console.log("app.auth config");
    $stateProvider
        .state('app.auth.login', {
            url: '^/login',
            views: {
                'main@app': {
                    templateUrl: "app/auth/login.html",
                    controller: "LoginController as loginCtrl"
                }
            },
            pageProp: {
                bodyClass: "page-login-v3",
                pageClass: "vertical-align text-xs-center",
                showMenu: false,
                showNavbar: false,
                showFooter: false,
                customStyles: [
                    '/assets/examples/css/pages/login-v3.css',
                ]
            }
        })
    ;
}