/*
 * login controller
 */

export default class LoginController {

    /*@ngInject;*/
    constructor(Auth, $state, $scope, $timeout) {
        this.Auth = Auth;
        this.$timeout = $timeout;

        this.loading = false;
        this.username = "";
        this.password = "";
        this.remember = false;
    }

    login() {
        let ctrl = this;
        this.loading = true;
        console.log("login clicked");
        this.Auth.Login(this.username, this.password, this.remember, function (result, response) {
            ctrl.loading = false;
            if (result === true) {
                ctrl.messages = ['Ure in'];
                // $state.go("app.inner-main.form.list");
            } else {
                console.log("response", response);
                if (response && response.status === -1) {
                    ctrl.errors = ['Something happened'];
                    ctrl.$timeout(()=>{
                        ctrl.errors.pop();
                    },5000);
                }else{
                    if(response.data && response.data.data.success && response.status =="200"){
                        // response.data.data.avatar_path
                        ctrl.messages = ['Ure in'];
                        console.log("errors from server");
                    }else{

                    console.log("errors from server");
                    ctrl.errors = [];
                    ctrl.dataIndexes = [];
                        for (var x in response.data.data.errors) {
                            if (response.data.data.errors.hasOwnProperty(x)) {
                                ctrl.dataIndexes.push(x);
                            }
                        }
                        for (var i = 0; i < ctrl.dataIndexes.length; i++){
                            for (var n = 0; n < response.data.data.errors[ctrl.dataIndexes[i]].length; n++){
                                ctrl.errors.push(response.data.data.errors[ctrl.dataIndexes[i]][n]);
                            }
                        }
                    }
                }
                // $scope.errors = [];
                // $scope.dataIndexes = [];
                // // console.log("response.data.errors", response.data.errors);
                // // FlashService.Error(response.message);
                // if (angular.isDefined(response.data.errors)) {
                //     for (var x in response.data.errors) {
                //         if (response.data.errors.hasOwnProperty(x)) {
                //             $scope.dataIndexes.push(x);
                //         }
                //     }
                //     for (var i = 0; i < $scope.dataIndexes.length; i++){
                //         for (var n = 0; n < response.data.errors[$scope.dataIndexes[i]].length; n++){
                //             $scope.errors.push(response.data.errors[$scope.dataIndexes[i]][n]);
                //         }
                //     }
                // }else{
                //     // console.log("Because")
                // }
                // $scope.loading = false;
            }
        });
    }
}
