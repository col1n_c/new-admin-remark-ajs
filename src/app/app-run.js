/**
 * Created by pasha on 15.02.17.
 */

/*@ngInject;*/
export default function runtime($state, $document, $rootScope, PageProperties, Auth, angularLoad) {
    console.log("runtime", arguments);

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        // console.log("%cROOT%cstate change started",'background-color: red;padding:3px 5px;font-weight: bold; color:white;border-radius:3px','background-color: green;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');


        let {stateAllowed, statePresent} = Auth.checkState(toState.name);

        if (stateAllowed && statePresent) {
            console.log("Allowed & Present");
            if (toState.pageProp) {
                console.log("pageProp Present");
                PageProperties.setProperties(toState.pageProp);
            }
        } else {
            console.log("Not Allowed");

            event.preventDefault();
            if (statePresent) {
                console.log("Present");
                $state.go("app.index");
            } else {
                console.log("Present");
                $state.go("app.404");
            }
        }

    });
    // $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
    //     console.log("%cROOT%cstate change ended",'background-color: red;padding:3px 5px;font-weight: bold; color:white;border-radius:3px','background-color: orange;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
    //     // controller.sectionClass = toState.stateClass;
    //     console.log("$arguments",arguments);
    // });
    // $rootScope.$on('$viewContentLoaded',function(event, toState, toParams, fromState, fromParams){
    // //     console.log("%cROOT%cviewContentLoaded",'background-color: red;padding:3px 5px;font-weight: bold; color:white;border-radius:3px','background-color: blue;padding:3px 5px;font-weight: bold; color:white;border-radius:3px;');
    // //     console.log("$arguments",arguments);
    // //     if (Site){
    // //         console.log("Site running");
    // //         Site.run();
    // //     }
    // });
}


