/**
 * Created by pasha on 16.02.17.
 */

angular.module("app.auth", []);
angular.module("app.crm", [
    'app.crm.user',
    'app.crm.dash'
]);
angular.module("app.crm.user", []);
angular.module("app.crm.dash", []);

