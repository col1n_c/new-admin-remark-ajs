// var angular = require('angular');
// var angularUiRouter = require('angular-ui-router');

//Controllers Modules
import HomeController from './home/HomeController';
import MainController from './layout/MainController';
import RootController from './layout/RootController';

//Services Modules
import NameService from './services/PersonService';

//Filters Modules
import {UpperFilter, LowerFilter} from './filters/textFilters';

//App Blocks Modules
import appConfig from './app-config';
import appRun from './app-run';

// Import all modules
import services from './services';

// Import all modules
import modules from './modules';

//Inport directives
import directives from './directives';

//App Init Modules
angular.module('app', [
    'app.core',
    'app.services',
    'app.directives',
    'app.main'
]);

angular.module('app.core', [
    'ui.router',
    'templates',
    'angularLoad'
])
/* @ngInject */
    .config(function ($locationProvider, $stateProvider, $urlRouterProvider, $interpolateProvider) {
        $stateProvider
            .state('app', {
                url: '',
                abstract: true,
                views: {
                    "root@": {
                        templateUrl: 'app/layout/root.html',
                        controller: "MainController as mainCtrl"
                    }
                },
                // has to be resolved by any child state, except app.auth
                resolve: {
                    /* ngInject */
                    siteLang: function ($http) {
                        return $http({
                            method: 'GET',
                            url: ('data/lang-list.json')
                        });
                    },
                    resA:  function () {
                        console.log("resolving as plane object");
                        return {
                            'value': 'A'
                        };
                    }
                }
            });
    })
    .controller('RootController', RootController)
    .controller('MainController', MainController)
;


angular.module("app.main", [
    "app.auth",
    "app.crm"
])
    .config(appConfig)
    .run(appRun)
    .controller('HomeController', HomeController)
    .service('PersonService', NameService)
    .filter('upper', UpperFilter)
    .filter('lower', LowerFilter)
;
//

import templates from './templates.js';
import auth from './auth/auth.module';
import crm from './crm/crm.module';
import user from './crm/user/user.module';
import dash from './crm/dash/dash.module';
import angularLoad from './components/vendor/angular-load';


