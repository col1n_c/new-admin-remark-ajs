/*
 You could also use a class for a controller, but if you work
 with $scope and not the controller-as syntax a function makes
 more sense, because most of the code would anyway go to the
 constructor if you would use a class.
 */

export default class HomeController {

    /*@ngInject;*/
    constructor(PersonService, $state) {
        console.log("Loaded controller", $state.current);
        PersonService.getPerson().then(person => {
            // person.name += "/n" + $state.current.name;
            this.person = person;
        });
    }

}
