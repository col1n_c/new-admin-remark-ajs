/**
 * Created by pasha on 15.02.17.
 */

/*@ngInject;*/
export default function configuration($stateProvider, $urlRouterProvider, $locationProvider) {
    console.log("configuring");

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
    });
    $stateProvider
        .state('app.index', {
            url: '/',
            views: {
                'main': {
                    templateUrl: "app/home/home.html",
                    controller: "HomeController as homeCtrl"
                }
            },
            pageProp: {
                bodyClass: "",
                pageClass: "vertical-align text-xs-center",
                showMenu: false,
                showNavbar: false,
                showFooter: false
            }
        })
        .state('app.404', {
            url: '^/404',
            views: {
                'main': {
                    templateUrl: "app/404/404.html",
                    controller: "HomeController as homeCtrl"
                }
            },
            pageProp: {
                bodyClass: "",
                pageClass: "vertical-align text-xs-center",
                showMenu: false,
                showNavbar: false,
                showFooter: false
            }
        })
        .state('app.crm', {
            url: '/crm',
            abstract: true,
            views: {
                'main': {
                    template: "",
                    // controller: "HomeController as homeCtrl"
                },
                'navbar': {
                    templateUrl: "app/crm/layout/common/nav-bar.html",
                    // controller: "HomeController as homeCtrl"
                },
                'menu': {
                    templateUrl: "app/crm/layout/common/menu-bar.html",
                    // controller: "HomeController as homeCtrl"
                },
                'grid-menu': {
                    templateUrl: "app/crm/layout/common/grid-menu.html",
                    // controller: "HomeController as homeCtrl"
                },
                'footer': {
                    templateUrl: "app/crm/layout/common/footer.html",
                    // controller: "HomeController as homeCtrl"
                }
            },
            pageProp: {
                bodyClass: "",
                pageClass: "",
                showMenu: true,
                showNavbar: true,
                showFooter: true,
                customStyles: [
                    // '/assets/examples/css/dashboard/v1.css',
                ]
            }
        })
        .state('app.auth', {
            url: '/auth',
            abstract: true,
            views: {
                'main': {
                    templateUrl: "",
                    controller: "HomeController as homeCtrl"
                }
            }
        })
        // .state('app.account', {
        //     url: '/account',
        //     views: {
        //         'main': {
        //             templateUrl: "app/account/main.html",
        //             controller: "AccountController as accountCtrl",
        //         }
        //     },
        //     pageProp: {
        //         // bodyClass: "page-login-v3 layout-full site-menubar-fold",
        //         // pageClass: "vertical-align text-xs-center",
        //         showMenu: true,
        //         showNavbar: true,
        //         showFooter: true
        //     },
        //     resolve: {
        //         /* @ngInject */
        //         accData: function ($state, $http) {
        //             return $http({
        //                 method: "GET",
        //                 url: "data/account.json"
        //             });
        //         },
        //         /* @ngInject */
        //         langList: function ($state, $http) {
        //             return $http({
        //                 method: "GET",
        //                 url: "data/lang-list.json"
        //             });
        //         }
        //     }
        // })
    ;
    $urlRouterProvider.otherwise("/404");
}


