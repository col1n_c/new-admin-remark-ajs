/*
 Here we connetc all our services to an app
 */

// Import services here
import PageService from './services/PageService';
import AuthService from './services/AuthService';

angular.module("app.services", [])
    .service('PageProperties',PageService)
    .service('Auth',AuthService)
;

