/**
 * Created by Xiaomi on 2/20/2017.
 */

describe('test filters', function() {
    beforeEach(module('app'));
    var $filter;
    var inputString;

    beforeEach(inject(function(_$filter_){
        inputString = "TeSt";
        $filter = _$filter_;
    }));

    it('returns lowercase string', function() {
        var length = $filter('lower');
        expect(length(inputString)).toMatch('test');
    });

    it('returns uppercase string', function() {
        var length = $filter('upper');
        expect(length(inputString)).toMatch('TEST');
    });
});