/*
 Here we connetc all our services to an app
 */

// Import directives here
import NameDirective from './components/name.directive';

//Layout directives
import MenuBarDirective from './crm/layout/common/menu-bar.directive';
import ASScrollableDirective from './layout/common/asScrollable.directive';
import {BootstrapToggles, BootstrapPlugins} from './layout/common/bootstrap-plugins.directive';
import BtnWaves from './layout/common/waves.directive';

angular.module("app.directives", [])
    .directive('nameDir', NameDirective)
    .directive('menuBar', MenuBarDirective)
    .directive('asScrollable', ASScrollableDirective)
    .directive('toggle', BootstrapToggles)
    .directive('plugin', BootstrapPlugins)
    .directive('btn', BtnWaves)
;

